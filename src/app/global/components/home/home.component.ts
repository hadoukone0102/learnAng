import { Component } from '@angular/core';
import { CorpsService } from 'src/app/corps/services/corps.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  constructor(
    private corpsService:CorpsService,
  ){}
  // goToHome(){this.corpsService.goToHome();}
}
