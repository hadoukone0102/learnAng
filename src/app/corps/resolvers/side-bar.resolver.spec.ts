import { TestBed } from '@angular/core/testing';
import { ResolveFn } from '@angular/router';

import { sideBarResolver } from './side-bar.resolver';

describe('sideBarResolver', () => {
  const executeResolver: ResolveFn<boolean> = (...resolverParameters) => 
      TestBed.runInInjectionContext(() => sideBarResolver(...resolverParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeResolver).toBeTruthy();
  });
});
