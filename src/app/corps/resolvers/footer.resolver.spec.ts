import { TestBed } from '@angular/core/testing';
import { ResolveFn } from '@angular/router';

import { footerResolver } from './footer.resolver';

describe('footerResolver', () => {
  const executeResolver: ResolveFn<boolean> = (...resolverParameters) => 
      TestBed.runInInjectionContext(() => footerResolver(...resolverParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeResolver).toBeTruthy();
  });
});
