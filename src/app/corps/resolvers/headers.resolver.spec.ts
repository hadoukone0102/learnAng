import { TestBed } from '@angular/core/testing';
import { ResolveFn } from '@angular/router';

import { headersResolver } from './headers.resolver';

describe('headersResolver', () => {
  const executeResolver: ResolveFn<boolean> = (...resolverParameters) => 
      TestBed.runInInjectionContext(() => headersResolver(...resolverParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeResolver).toBeTruthy();
  });
});
