import { ResolveFn } from '@angular/router';

export const sideBarResolver: ResolveFn<boolean> = (route, state) => {
  return true;
};
