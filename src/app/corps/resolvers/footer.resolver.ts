import { ResolveFn } from '@angular/router';

export const footerResolver: ResolveFn<boolean> = (route, state) => {
  return true;
};
