import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeadersComponent } from './components/headers/headers.component';
import { SideBarComponent } from './components/side-bar/side-bar.component';
import { FooterComponent } from './components/footer/footer.component';



@NgModule({
  declarations: [
    HeadersComponent,
    SideBarComponent,
    FooterComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    HeadersComponent,
    SideBarComponent,
    FooterComponent
  ]
})
export class CorpsModule { }
