import { Component } from '@angular/core';
import { CorpsService } from '../../services/corps.service';

@Component({
  selector: 'app-headers',
  templateUrl: './headers.component.html',
  styleUrls: ['./headers.component.css']
})
export class HeadersComponent {
    constructor(
      private corpsService:CorpsService,
    ){}
    goToHome(){this.corpsService.goToHome();}
}
