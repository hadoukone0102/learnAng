import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeadersComponent } from './corps/components/headers/headers.component';
import { HomeComponent } from './global/components/home/home.component';

const routes: Routes = [
  {path: 'home', component:HomeComponent},
  {path: '**', redirectTo:'home', pathMatch:"full"},
  {path: '', redirectTo:'home', pathMatch:"full"},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
