import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CorpsModule } from './corps/corps.module';
import { PlaysModule } from './plays/plays.module';
import { FormulaireModule } from './formulaire/formulaire.module';
import { GlobalModule } from './global/global.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CorpsModule,
    PlaysModule,
    FormulaireModule,
    GlobalModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
